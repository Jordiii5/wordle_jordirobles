import java.util.*
fun main(args: Array<String>) {
    // Variables dels colors
    val fondoverd = "\u001B[42m"
    val fondogroc = "\u001B[43m"
    val blanc = "\u001B[1;97m"
    val reset = "\u001B[0m"
    val fondo = "\u001B[47m"

    //Interacció amb l'usuari
    println("")
    println("BENVINGUT/A AL MÓN TERRORIFIC DE LES PARAULES, PER PODER SORTIR D'AQUEST ESPANTÓS LLOC, HI HAURÀS DE GUANYAR AL JOC DEL WORDLE!")
    println("Aquest joc consisteix a trobar una paraula a partir dels encerts i els errors comesos en les seves lletres.")
    println("")
    println("☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆ REGLES ☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★")
    println("")
    println("1- Si la lletra es mostra de color $fondoverd$blanc verd $reset és correcta.")
    println("2- Si la lletra es mostra de color $fondogroc$blanc groc $reset, la lletra està dins de la paraula però en posició incorrecta.")
    println("3- Si la lletra no està acolorida significa que la lletra no es troba en la paraula.")
    println("4- Tens 6 intents.")
    println("5- La paraula introduida ha de ser de 5 lletres.")
    println("6- La paraula introduïda ha de ser en català.")
    println("7- La paraula introduïda no pot tenir cap lletra repetida.")
    println("MOLTA SORT!")
    println("")
    println("☆★☆★☆★☆★☆★☆★☆★☆★☆★☆ VOLS SEGUIR AMB LA TEVA VIDA? DONCS POSA UNA PARAULA PER COMENÇAR A JUGAR ☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★")
    println("")
    
    //variables de la partida
    val scanner=Scanner(System.`in`)
    var vides = 6
    val paraulaArray = arrayOf("amunt", "burla", "calor", "donar", "espai", "funda", "guapo", "humor", "indol", "jugar", "lunar", "music", "nebot", "ombra", "punxa", "quilo", "rumba", "sumar", "tumor", "urani", "volum", "xines", "zebra").random()
    val mutableList: MutableList<String> = mutableListOf()
    val abecedari: MutableList<Char> = mutableListOf('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z')
    
    //Lógica de la partida
    do {
        val paraulaUsuari:String = scanner.next()
        while (paraulaUsuari.length != 5){
            println("No pots posar una paraula amb + o - de 5 caracters")
            println("Intenta amb altre paraula amb 5 caracters")
            break
        }
        while(paraulaUsuari.length == 5){
            for (i in 0..4){
                if (paraulaUsuari[i] == paraulaArray[i]){
                    print("│")
                    print(fondoverd +"$blanc ${paraulaUsuari[i]} $reset")
                    print("│")
                }else if (paraulaUsuari[i] in paraulaArray){
                    print("│")
                    print(fondogroc+"$blanc ${paraulaUsuari[i]} $reset")
                    print("│")
                }else if (paraulaUsuari[i] !in paraulaArray){
                    print("│")
                    print(fondo+"$blanc ${paraulaUsuari[i]} $reset")
                    print("│")
                }
                if (paraulaUsuari[i] in abecedari){
                    abecedari.remove((paraulaUsuari[i]))
                }
            }
            vides--

            if (vides!=0 && paraulaUsuari!=paraulaArray){
                mutableList.add(paraulaUsuari)

                println("$reset\nEt queden $vides vides")
                println(mutableList)
                println(abecedari)
            }else if (paraulaUsuari==paraulaArray){
                println("$reset\nFELICITATS HAS GUANYAT!!!")
            }else{
                println("$reset\nHAS PERDUT JAJA BOOMER")
                print("$reset\nLa paraula desconeguda era $fondoverd$blanc$paraulaArray")
            }
            break
        }
    }while (paraulaUsuari != paraulaArray && vides != 0)
}