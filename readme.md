<h1>WORDLE</h1>
<hr>
<h3>Descripció del projecte</h3>
<p>Wordle és un joc d'endevinar paraules, que té un format de mots encreuats i amb similituds amb altres jocs com el Mastermind. En ell, has d'endevinar una paraula en sis intents, en els quals no se't donen més pistes que dir-te quines lletres de les quals has posat estan dins de la paraula.</p>
<h3>Regles</h3>
<ol>
<li>Si la lletra es mostra de color verd és correcta.</li>
<li>Si la lletra es mostra de color groc, la lletra està dins de la paraula però en posició incorrecta</li>
<li>Si la lletra no està acolorida significa que la lletra no es troba en la paraula.</li>
<li>Tens 6 intents.</li>
<li>La paraula introduida ha de ser de 5 lletres.</li>
<li>La paraula introduïda ha de ser en català.</li>
<li>La paraula introduïda no pot tenir cap lletra repetida.</li>
</ol>
<h3>Instruccions del joc</h3>
<ul>
<li>El jugador escriu a la primera línia una paraula de cinc lletres de la seva elecció i introdueix la seva proposta. </li>
<li>Després de cada proposició, les lletres apareixen en color:
<ul>
<li>El <strong>fons gris</strong> representa les lletres que no estan a la paraula cercada.
</li>
<li>El <strong>fons groc</strong> representa les lletres que es troben en altres llocs de la paraula.
</li>
<li>El <strong>fons verd</strong> representa les lletres que estan al lloc correcte en la paraula a trobar.</li>
</ul>
</li>
</ul>
<h3>Instal·lació i execució</h3>
<p>Per a executar el projecte primer de tot s'ha de descarregar el projecte, per a això dirigeix-te a la terminal i executa la següent comanda:</p>
<p>git clone (Introdueix aqui el link del proyecte)</p>
<p>Una vegada fet això s'haurà descarregat el projecte, el següent pas serà obrir IntelliJ i dins seleccionem l'opció open proyect per a seleccionar el projecte recentment descarregat.
Una vegada dins del projecte ens dirigim al fitxer del proyecte, dins d'aquest fitxer podrem veure el codi i per a executar-lo fem clic en el botó executar a la cantonada superior dreta</p>
<h3>Llicencia</h3>
<p>MIT License</p>
<p>Copyright (c) [2022] [Jordi Robles Perera]</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.</p>